var path = require('path')
const htmlWPP = require('html-webpack-plugin')

module.exports = {
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'prod'),
        filename: 'index.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                },
            },
            {
                test: /\.(css)$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ],
    },
    devServer: {
        historyApiFallback: true,
    },
    plugins: [
        new htmlWPP({
            template: path.resolve(__dirname, 'index.html')
        })
    ]
}
import React, { useEffect, useState } from 'react'
import Product from './Product'

async function getData(){
    var products
    // https://scandiweb-test-php.000webhostapp.com/
    await fetch('http://localhost/server-work1/index.php',{
        method: 'GET',
        mode: 'cors',
        withCredentials: true,
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then((res)=> {
        return res.json()
    })
    .then((data)=> {
        products = data
    })
    return products
}

// Props:
// 1. onChecked is a prop listens for checkboxes, if checked they're added to a list to delete later when clicked on the button.
// 2. productsChange is a prop passes from parent to re-get products from the database when some products are deleted.
// States:
// products, it's the list of products ready to be shown.

const Products = ({onChecked, productsChange}) => {

    var [products,setProducts] = useState([])
    useEffect(()=> {
        getData().then((productsList)=> {
            setProducts(productsList)
            return
        })
    }, [productsChange])

    return (
        <div className="row-flex">
        {
            (products.length != 0) ?
            (products[0].length != 0 || products[1].length != 0 || products[2].length != 0) ?
            products.map((productType)=>(
                productType.map((product)=>(
                        <Product key={product.PRODUCT_ID} ID={product.PRODUCT_ID} SKU={product.SKU} name={product.NAME} type={product.TYPE} price={product.PRICE} value={product.VALUE} measurement={product.measurement} onChecked={onChecked}/>
                    ))
            )) : <p>No items, click the + button to add some!</p>
            : <p>No items, click the + button to add some!</p>
        }
        </div>
    )
}

export default Products

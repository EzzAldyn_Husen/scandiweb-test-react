import React from 'react'

const Product = ({ ID, SKU, name, price, type, value, measurement, onChecked }) => {
    // var [checked, isChecked] = useState(false)
    return (
        <div className="column-flex" id="product-card">
                <input type="checkbox" className="delete-checkbox" onClick={(e)=> {
                    onChecked(ID)
                }}/>
            <div className="column-flex" id="product-description">
                <h3>{SKU}</h3>
                <h4>{name}</h4>
                <p>{price} $</p>
                <p>{type}: {value} {measurement}</p>
            </div>
        </div>
    )
}

export default Product

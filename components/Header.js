import React from 'react'

const Header = ({onDelete, addProduct}) => {
    return (
        <header id="header-hero">
            <h1>{addProduct ? 'Add product' : 'Product list'}</h1>
            <div>
                <button className="button-header" form={addProduct ? 'product_form' : ''} type={addProduct ? "submit" : ""} onClick={()=> (!addProduct ? window.location.assign('/add-product') : '')}> {addProduct ? 'Save' : 'ADD'}</button>
                <button className="button-header" id="delete-product-btn" onClick={()=> (addProduct ? window.location.assign('/') : onDelete())}>{addProduct ? 'Cancel' : 'MASS DELETE'}</button>
            </div>
        </header>
    )
}

export default Header

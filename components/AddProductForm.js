import React, { useState } from 'react'
import { useForm } from 'react-hook-form'

const AddProductForm = () => {

    // a state for when duplicate "SKU" is entered into the system
    const [isDuplicateSKU,setDuplicateSKU] = useState(false)

    // Those are for making sure only shown elements are required, others not shown are not required!
    const [isSizeShown, setSizeShown] = useState(false)
    const [isWeightShown, setWeightShown] = useState(false)
    const [isDimensionShown, setDimensionShown] = useState(false)

    const submit = async data => {
        var newData = {...data}

        if(data.size != ''){
            newData.value = data.size
        }
        else if(data.weight != ''){
            newData.value = data.weight
        }
        else {
            newData.value = `${data.height}x${data.width}x${data.length}`
        }
        delete newData.size
        delete newData.weight
        delete newData.height
        delete newData.width
        delete newData.length
        // https://scandiweb-test-php.000webhostapp.com/
        await fetch('http://localhost/server-work1/index.php', {
            method: 'POST',
            mode: 'cors',
            withCredentials: true,
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newData)
        }).then((res)=> {
            return res.json()
        })
        .then((data)=> {
            if(data === `Duplicate entry '${newData.SKU}' for key 'sku_name'`){
                setDuplicateSKU(true)
            }
            else if (data == '1'){
                window.location.assign('/')
            }
        })
    }
    // showProductTypeInput: shows hidden inputs depending on the chosen type.
    const showProductTypeInput = (e)=>{
            document.querySelectorAll('.type_choice').forEach((element)=> {
                element.style.display = 'none'
            })
            switch (e.target.value) {
                case 'Size':
                    setSizeShown(true)
                    setWeightShown(false)
                    setDimensionShown(false)
                    break;
                
                case 'Dimensions':
                    setSizeShown(false)
                    setWeightShown(false)
                    setDimensionShown(true)
                    break;
                
                case 'Weight':
                    setSizeShown(false)
                    setWeightShown(true)
                    setDimensionShown(false)                                    
                    break;
            }
            const element = document.getElementById(`${e.target.value}-container`)
            if(element != null)
            element.style.display = 'block'
    }
    const { register, handleSubmit, formState: {errors} } = useForm()
    return (
        <div id="form_container">
            <form id="product_form" onSubmit={handleSubmit(submit)}>

                <div className="input-container">
                    <label htmlFor="sku">SKU</label>
                    <input name="sku" type="text" id="sku" {...register("SKU", {required: true})} placeholder="i.e. JCV032ASX" onChange={()=> (setDuplicateSKU(false))}/>
                </div>
                    { (errors.SKU) && (errors.SKU.type) == "required" ? (<p className="error" id="SKU-err">Please, submit required data</p>) : '' }
                    { (isDuplicateSKU) ? (<p className="error" id="SKU-err">A product with this SKU already exists</p>) : '' }
                    
                <div className="input-container">
                    <label htmlFor="name">Name</label>
                    <input name="name" type="text" id="name" {...register("name", {required: true})} placeholder="Type the name here..."/>
                </div>
                { (errors.name) && (errors.name.type) == "required" ? (<p className="error" id="name-err">Please, submit required data</p>) : '' }

                <div className="input-container">
                    <label htmlFor="price">Price ($)</label>
                    <input name="price" type="number" step="0.01" id="price" {...register("price", {required: true})} placeholder="Choose the price"/>
                </div>
                { (errors.price) && (errors.price.type) == "required" ? (<p className="error" id="price-err">Please, submit required data</p>) : '' }

                <div className="input-container">
                    <label htmlFor="types">Type switcher</label>
                    <select title="Select type" name="types" {...register("type", {required: true})} placeholder="Which type ?" id="productType" onClick={(e)=> (showProductTypeInput(e))} onChange={(e)=> (showProductTypeInput(e))}>
                        <option value="Size">DVD</option>
                        <option value="Dimensions">Furniture</option>
                        <option value="Weight">Book</option>
                    </select>
                </div>
                { (errors.type) && (errors.type.type) == "required" ? (<p className="error" id="types-err">Please, submit required data</p>) : '' }

                {/* Special place that is controlled based on the choice of the type */}
                <div className="type_choice" id="Size-container">
                    <div className="input-container">
                        <label htmlFor="size">Size (MB)</label>
                        <input name="size" type="number" step="0.1" {...register("size", {required: isSizeShown ? true : false})} id="size" placeholder="Type the size here..."/>
                    </div>
                    { (errors.size) && (errors.size.type) == "required" ? (<p className="error" id="size-err">Please, submit required data</p>) : '' }
                    <p id="input-description">Please provide size in MB format.</p>
                </div>

                <div className="type_choice" id="Weight-container">

                    <div className="input-container">
                        <label htmlFor="weight">Weight (KG)</label>
                        <input name="weight" type="number" step="0.01" {...register("weight", {required: isWeightShown ? true : false})} id="weight" placeholder="Type the weight here..."/>
                    </div>
                    { (errors.weight) && (errors.weight.type) == "required" ? (<p className="error" id="weight-err">Please, submit required data</p>) : '' }
                    <p id="input-description">Please provide weight in KG format.</p>
                </div>

                <div className="type_choice" id="Dimensions-container">
                    <div className="input-container">
                        <label htmlFor="height">Height (CM)</label>
                        <input name="height" type="number" step="0.01" {...register("height", {required: isDimensionShown ? true : false})} id="height" placeholder="Type the height here..."/>
                    </div>
                    { (errors.height) && (errors.height.type) == "required" ? (<p className="error" id="height-err">Please, submit required data</p>) : '' }
                    <p id="input-description">Please provide height in cm format.</p>

                    <div className="input-container">
                        <label htmlFor="width">Width (CM)</label>
                        <input name="width" type="number" step="0.01" {...register("width", {required: isDimensionShown ? true : false})} id="width" placeholder="Type the width here..."/>
                    </div>
                    { (errors.width) && (errors.width.type) == "required" ? (<p className="error" id="width-err">Please, submit required data</p>) : '' }
                    <p id="input-description">Please provide width in cm format.</p>

                    <div className="input-container">
                        <label htmlFor="length">Length (CM)</label>
                        <input name="length" type="number" step="0.01" {...register("length", {required: isDimensionShown ? true : false})} id="length" placeholder="Type the length here..."/>
                    </div>
                    { (errors.length) && (errors.length.type) == "required" ? (<p className="error" id="length-err">Please, submit required data</p>) : '' }
                    <p id="input-description">Please provide length in cm format.</p>
                    
                </div>

            </form>
        </div>
    )
}

export default AddProductForm

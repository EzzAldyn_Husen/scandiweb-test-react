import React, { lazy, Suspense, useState } from 'react'
import ReactDOM from 'react-dom'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import Header from './components/Header'
import Footer from './components/Footer'
import AddProductForm from './components/AddProductForm'
const Products = lazy(()=> import('./components/Products'))

import './style/styles.css'

// list of checked products, contains the primary keys of checked products,
// when deleting it sends the keys to server.
var checkedProducts = []

const Index = () => {

    const setCheckedProducts = (key) => {
        var index = checkedProducts.indexOf(key)
        index == -1 ? checkedProducts.push(key) : checkedProducts.splice(index,1)
        showErrorNoChecked(false)
    }
    const deleteProducts = async ()=> {
        if(checkedProducts.length == 0){
            showErrorNoChecked(true)
            return
        }
        showErrorNoChecked(false)
        // https://scandiweb-test-php.000webhostapp.com/
        await fetch('http://localhost/server-work1/index.php', {
            method: 'POST',
            mode: 'cors',
            withCredentials: true,
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                DELETE: checkedProducts
            })
        }
        ).then((res)=> 
            {return res.json()}).then((data)=>
                {
                    data === true ? (setProductsChanged(!didProductsChange), checkedProducts = []) : console.log(data)
            }
        )
    }
    
    // some error states
    const [noChecked, showErrorNoChecked] = useState(false)
    const [didProductsChange, setProductsChanged] = useState(false)
    return (
        <>
        <Router>
            <Switch>
                <Route path="/" exact>
                    <>
                        <Header onDelete={deleteProducts}/>
                        {
                        noChecked ?
                        <div id="error-nochecked-header">
                            <p>First select something to delete!</p>
                        </div> : <></>
                        }
                        <Suspense fallback={<div id="loading">Loading...</div>}>
                           <Products onChecked={setCheckedProducts} productsChange={didProductsChange}/>
                            <div id="space-below"></div>
                        </Suspense>
                    </>
                </Route>
                <Route path="/add-product">
                    <Header addProduct={true}/>
                    <AddProductForm/>
                </Route>
            </Switch>
        </Router>
        <Footer/>
        </>
    )
}

ReactDOM.render(<Index/>, document.getElementById('root'))